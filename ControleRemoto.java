/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.bridge;

/**
 *
 * @author janio.silva
 */
public abstract class ControleRemoto {
    private Dispositivo disp;
    
    public ControleRemoto(Dispositivo novo){
        this.disp=novo;
    }
    public void botaoCinco(){
        disp.botaoCinco();
    }
    public void botaoSeis(){
        disp.botaoSeis();
    }
    public abstract void botaoNove();
    
}
