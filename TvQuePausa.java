/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.bridge;

/**
 *
 * @author janio.silva
 */
public class TvQuePausa extends ControleRemoto{

    public TvQuePausa(Dispositivo novo){
        super(novo);
    }
    
    @Override
    public void botaoNove() {
        System.out.println("TV Ficou Pausada");
    }
    
}
