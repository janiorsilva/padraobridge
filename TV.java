/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.bridge;

/**
 *
 * @author janio.silva
 */
public class TV extends Dispositivo{

     public TV(int estadoAtual, int estadoMaximo){
         estado=estadoAtual;
         maximo=estadoMaximo;
     }
     
    @Override
    public void botaoCinco() {
        estado--;
        System.out.println("Canal Down: "+estado);
    }

    @Override
    public void botaoSeis() {
        estado++;
        System.out.println("Canal UP: "+estado);
    }
    
}
