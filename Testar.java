/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.bridge;

/**
 *
 * @author janio.silva
 */
public class Testar {
    
    public static void main(String[] args) {
        //Testar testar = new Testar();
        ControleRemoto tv1 = new TvQueFicaMuda(
                new TV(1,100));
        ControleRemoto tv2 = new TvQuePausa(
                new TV(1,200));
        //Para casa: adicionar um XBOX, PS5 e um Blue Ray
        System.out.println("Primeiro: TV QUE FICA MUDA:");
        tv1.botaoCinco();tv1.botaoSeis();tv1.botaoNove();
        //testar.testarInterface(tv1);
        System.out.println("Depois: TV QUE PAUSA:");
        tv2.botaoCinco();tv2.botaoSeis();tv2.botaoNove();
        //testar.testarInterface(tv2);
        
    }
    
    public void testarInterface(ControleRemoto controle){
        controle.botaoCinco();
        controle.botaoSeis();
        controle.botaoNove();
    }
    
}
