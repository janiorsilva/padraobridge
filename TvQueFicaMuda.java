/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.bridge;

/**
 *
 * @author janio.silva
 */
public class TvQueFicaMuda extends ControleRemoto  {

    public TvQueFicaMuda(Dispositivo novo){
        super(novo);
    }
    
    @Override
    public void botaoNove() {
        System.out.println("TV ficou Muda!");
    }
    
}
