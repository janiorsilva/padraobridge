/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.bridge;

/**
 *
 * @author janio.silva
 */
public abstract class Dispositivo {
    public int estado;
    public int volume;
    public int maximo;
    
    public abstract void botaoCinco();
    public abstract void botaoSeis();
    
    public void botaoSete(){
        volume++;
        System.out.println("Volume em: "+volume);
    }
    public void botaoOito(){
        volume--;
        System.out.println("Volume em: "+volume);
    }
    
}
